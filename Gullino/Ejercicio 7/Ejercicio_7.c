/* Construya una función llamada "primo" que reciba un entero positivo y devuelva un 1 si es primo, o un 0 si no lo es. 
Un número positivo primo solamente tiene 2 divisores: el 1 y si mismo. 
Llame desde el main a esta función para probar que funciona correctamente. */

#include <stdio.h>
int primo();

int main()
{
    int numero = 0;
    int numero_primo = 0;
    printf("Ingrese un numero: ");
    scanf("%d", &numero);
    numero_primo = primo (numero);
    if(numero_primo == 1)
    {
        printf("1");
    }
    else
    {
        printf("0");
    }
    return(0);
}

int primo(int num)
{
    int contador_divisor = 0;
    int divisor;
    for(divisor = 1 ; divisor <= num ; divisor++)
    {
        if(num % divisor == 0)
        {
            contador_divisor++;
        }
    }
        if(contador_divisor == 2)
        {
            return 1;
        }
        else
        {
            return 0;
        }
}