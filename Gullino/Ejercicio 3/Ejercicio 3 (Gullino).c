
/* Mostrar en pantalla los números del 1 al 30 inclusive, pero solamente los pares. Usar bucles. */

#include <stdio.h>

int main (void)
{
    int numeros = 0;
    int pares = 0;
    
    for(numeros = 1; numeros <= 30; numeros++)
    {
        if(numeros % 2 == 0)
        {
            printf("%d\n", numeros);
        }
    }
    return(0);
}