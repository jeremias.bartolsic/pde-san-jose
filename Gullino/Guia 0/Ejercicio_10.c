/* El usuario ingresará números float positivos o negativos. 
Cuando ingrese el cero deberá finalizar el programa. 
Al finalizar, mostrar en pantalla si alguna vez el usuario ingresó algún número negativo. 
No importa la cantidad ni el valor sino si lo hizo o no. */
#include <stdio.h>
float numeros = 1;
int negativo = 0;
int main()
{
    while(numeros != 0)
    {
        scanf("%f", &numeros);
        if(numeros < 0)
        {
            negativo++;
        }
    }
    if(negativo > 0)
    {
        printf("\nSe utilizo un numero negativo\n");
    }
    return(0);
}
