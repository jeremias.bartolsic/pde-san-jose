/* Pida al usuario números enteros positivos. El usuario indicará que quiere finalizar ingresando el valor -1. 
No sabemos cuántos números ingresará el usuario. Al finalizar simplemente muestre en pantalla la palabra "FIN". */

#include <stdio.h>
int numeros = 0;
int main()
{
    while(numeros != -1)
    {
        scanf("%d", &numeros);
    }
    printf("FIN");
    return(0);
}