/* Al ejercicio anterior, agregue que al finalizar el programa se muestre la cantidad de números ingresados (hay que contarlos). */

#include <stdio.h>
int numeros = 0;
int cantidad_numeros = 0;
int main()
{
    while(numeros != -1)
    {
        scanf("%d", &numeros);
        cantidad_numeros++;
    }
    printf("\nFIN\n");
    printf("\nSe ingresaron %d numeros\n", cantidad_numeros);
    return(0);
}