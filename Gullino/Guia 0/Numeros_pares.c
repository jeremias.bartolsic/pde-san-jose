/* 5) El usuario ingresará números enteros positivos (no sabemos cuántos), hasta que ingrese el número -1 para
indicar que finaliza el ingreso. Mostrar en pantalla cuántos números pares ingresó. */

#include<stdio.h>

int numeros = 0;
int cantidad_pares = 0;

int main()
{
    while(numeros != -1)
    {
        scanf("%d", &numeros);
        if(numeros % 2 == 0)
        {
            cantidad_pares++;
        }
    }
    printf("Se ingresaron %d numeros pares", cantidad_pares);
    return(0);
}