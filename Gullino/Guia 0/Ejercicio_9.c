/* Al ejercicio anterior, agregue que al finalizar se muestre cuál fue el número más grande ingresado. */

#include <stdio.h>
int numeros, mayor, cantidad_numeros = 0;
int main()
{
    while(1)
    {
        scanf("%d", &numeros);
        if(numeros == -1)
        {
            break;
        }
        cantidad_numeros++;
        mayor = numeros;
        if(numeros > mayor)
        {
            mayor = numeros;
        }
    }
    printf("\nFIN\n");
    printf("Se ingresaron %d numeros\n", cantidad_numeros);
    printf("El numero mayor fue %d\n", mayor);
    return(0);
}