/* Pida al usuario tres números enteros. Muestre en pantalla el mayor de los tres. */

#include<stdio.h>
#define total_numeros 3
int numero = 0;
int mayor = 0;
int cantidad_numeros = 1;
int main()
{
    while(cantidad_numeros <= total_numeros)
    {
        scanf("%d", &numero);
        mayor = numero;
        cantidad_numeros++;
        if(numero > mayor)
        {
            mayor = numero;
        }
    }
    printf("El numero mayor fue: %d", mayor);
    return(0);
}