/* Pida al usuario un número entero e imprima en pantalla la palabra "positivo" 
si el número ingresado es igual o mayor a cero, o "negativo" en caso contrario. */

#include <stdio.h>
int numero;

int main ()
{
    scanf("%d", &numero);
    if(numero >= 0)
    {
        printf("\nPositivo\n");
    }
    else
    {
        printf("\nNegativo\n");
    }
    return(0);
}