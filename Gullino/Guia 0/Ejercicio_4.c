/* Pida al usuario un número entero e imprima en pantalla la palabra "hola" tantas veces como ese entero indique. */

#include <stdio.h>
int numero;
int total_hola = 1;
int main()
{
    scanf("%d", &numero);
    while(total_hola <= numero)
    {
        printf("\nHola\n");
        total_hola++;
    }
    return(0);
}