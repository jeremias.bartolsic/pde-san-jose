/* Solicite al usuario dos números enteros y calcule su promedio.
Muestre el promedio en pantalla.
Verifique que los resultados son correctos. Por ejemplo, 5 y 6 tiene que dar 5.5 */

#define total_numeros 2
#include<stdio.h>
int numeros, suma, cantidad_numeros = 0;
float promedio = 0;
int main()
{
    while(cantidad_numeros < total_numeros)
    {
        printf("Ingrese sus numeros\n");
        scanf("%d", &numeros);
        suma = (float) numeros + suma;
        cantidad_numeros++;
    }
    promedio = (float) suma / total_numeros;
    printf("%f", promedio);
    return(0);
}