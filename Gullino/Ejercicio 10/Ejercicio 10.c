/* 
Dibuje en pantalla el siguiente triángulo rectángulo, utilizando bucles.
 El usuario ingresa un número que se tomará como el tamaño de los catetos.
 El siguiente ejemplo sería con cateto 5:

*
**
***
****
***** 
*/

#include <stdio.h>
int main ()
{
    int tamanio = 0, i = 0, j = 0;

    printf("\nIngrese longitud\n");
    scanf("%d", &tamanio);
    for(i = 1; i <= tamanio; i++)
    {
        for(j = 1; j <= i; j++)
        {
            printf("* ");
        }
        printf("\n");
    }
    return(0);
}



