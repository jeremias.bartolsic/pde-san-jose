/* El usuario ingresará números enteros positivos (no sabemos cuántos), hasta que
ingrese el número -1 para indicar que finaliza el ingreso. Mostrar en pantalla cuántos números pares ingresó */

#include <stdio.h>

int main ()
{
    int numero = 0;
    int total_pares = 0;
    
    while(numero != -1)
    {   
        printf("Ingrese sus numeros\n");
        scanf("%d", &numero);
        if(numero % 2 == 0)
        {
            total_pares++;
        }
    }
    printf("Se ingresaron %d numeros pares", total_pares);
    return(0);
}