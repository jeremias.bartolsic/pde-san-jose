/* Ingresar numeros e indicar por orden cuales son pares y cuales no */
#include <stdio.h>
#define maximo_numeros 100
int main()
{
    int numeros = 0;
    int contador_numeros = 1;
    int suma_pares = 0;
    int suma_impares = 0;
    int contador_pares = 0;
    int contador_impares = 0;
    while(contador_numeros <= maximo_numeros)
    {
        printf("Ingrese numero\n");
        scanf("%d", &numeros);
        if(contador_numeros % 2 == 0)
        {
            contador_pares++;
            suma_pares = suma_pares + numeros;
        }
        else
        {
            contador_impares++;
            suma_impares = suma_impares + numeros;
        }
        contador_numeros++;
    }
    printf("\nHay un total de %d numeros en posicion par y %d en impar\n", contador_pares, contador_impares);
    if(suma_pares > suma_impares)
    {
        printf("\nLa suma de los numeros en posicion par es mayor que los impares\n");
    }
    if(suma_pares < suma_impares)
    {
        printf("\nLa suma de los numeros en posicion impar es mayor que los pares\n");
    }
    return(0);
}