/*
 El usuario ingresará números enteros menores a 100 (no sabemos cuántos).
Cuando ingrese un número mayor a 100 debe finalizar el ingreso.
Luego el programa debe mostrar cuántos números menores a 50 y cuántos mayores a 50 se han ingresado, y terminar. 
*/
#include <stdio.h>
int main()
{
    int numeros = 0, mayores = 0, menores = 0;
    while(numeros <= 100)
    {
        scanf("%d", &numeros);
        if(numeros > 100)
        {
            break;
        }
        if(numeros > 50)
        {
            mayores++;
        }
        if(numeros < 50)
        {
            menores++;
        }
    }
    printf("%d numeros son mayores a 50 y %d son menores", mayores, menores);
    return(0);
}
