/*
El usuario ingresa 6 números con decimales.
 El programa debe mostrar, luego de hacer los ingresos, cuál fue el número más grande que se ingresó y el promedio de todos los ingresados.
*/

#include <stdio.h>
#define maximo_numeros 6
int main()
{
    float numeros = 0, mayor = 0, promedio = 0, suma = 0;
    int contador_numeros = 1;
    while(contador_numeros <= maximo_numeros)
    {
        scanf("%f", &numeros);
        suma = numeros + suma;
        if(numeros > mayor)
        {
            mayor = numeros;
        }
        contador_numeros++;
    }
    promedio = suma / maximo_numeros;
    printf("El numero mayor fue %f y el promedio total es de %f", mayor, promedio);
    return(0);
}

