#include<stdio.h>
#define cantidad_materias 20

int main()
{
    int materia = 0, mejor_materia = 0, contador_materias = 1;
    float nota = 0, mejor_nota = 0;
    while(contador_materias < cantidad_materias)
    {
        scanf("%d", &materia);
        scanf("%f", &nota);
        if(nota > mejor_nota)
        {
            mejor_nota = nota;
            mejor_materia = materia;
        }
        contador_materias++;
    }
    printf("La mejor nota fue de %f en la materia %d", mejor_nota, mejor_materia);
    return(0);
}