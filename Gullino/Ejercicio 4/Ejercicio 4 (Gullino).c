
// Mostrar en pantalla los números del 10 al 48 inclusive, pero solamente los múltiplos de 3. Usar bucles.

#include <stdio.h>
//Variables



//Codigo
int main (void)
{
    int numeros = 0;
    
    for(numeros = 10; numeros <= 48; numeros++)
    {
        if(numeros % 3 == 0)
        {
            printf("%d\n", numeros);
        }
    }
    return(0);
}