/* El usuario ingresará un entero positivo. Imprima en pantalla todos sus divisores.*/

#include <stdio.h>

int main (void)
{
    int numero = 0;
    int divisor = 1;

    printf("Ingrese el numero: ");
    scanf("%d", &numero);

    while(numero >= divisor)
    {
        if(numero % divisor == 0)
        {
            printf("%d\n", divisor);
        }
        divisor++;
    }
    return(0);
}