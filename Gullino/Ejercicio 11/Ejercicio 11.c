/*Dibuje el mismo triángulo pero al revés. El siguiente ejemplo sería con cateto 4:

   *
  **
 ***
**** */
#include <stdio.h>
int main() 
{
    int i = 0, j = 0, tamanio = 0;
    printf("Ingrese tamanio ");
    scanf("%d", &tamanio);
    for(i = 1; i <= tamanio; i++)
    {
        for(j = i; j < tamanio; j++)
            {
                printf(" ");
            }
        for(j = 1; j <= i; j++)
            {
                printf("*");
            }
        printf("\n");
    }
    return(0);
}