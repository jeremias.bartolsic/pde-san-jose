/* Declare un vector de 10 enteros y carguelo con 1 y 0 alternativamente. 
Es decir, en la primer posición un 1, en la segunda un 0, en la tercera un 1, en la cuarta un 0... etc. 
Muestrelo en pantalla para verificar que quedó bien cargado. */
#include <stdio.h>
int main()
{
    int enteros [10], contador_unos = 0, i = 1, a = 1;
    for(i = 1; i <= 10; i++)
    {
        enteros[i] = 0;
        if(contador_unos >= 2)
        {
            contador_unos = 0;
        }
        contador_unos++;
        if(contador_unos == 1)
        {
            enteros[i] = 1;
        }
        printf("\n%d\n", enteros[i]);
    }
    return(0);
}