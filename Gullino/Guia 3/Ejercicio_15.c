/* Dado un vector de 10 enteros, imprima sus valores en pantalla sin repetirlos. 
Es decir que si un valor aparece más de una vez solo hay que imprimirlo una. */
#include <stdio.h>
int main()
{
    int numeros [10], i = 0, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0, j = 0, k = 0, contador_repetidos = 0;
    for(i = 0; i < 10; i++)
    {
        numeros[i] = 0;
        printf("\nIngrese valor\n");
        scanf("%d", &numeros[i]);
    }
    i = 0;
    while(1)
    {
        if(a == 0)a++;
        if(numeros[0] == numeros[a])
        {
            contador_repetidos++;
        }
        if(b == 1)b++;
        if(numeros[1] == numeros[b])
        {
            contador_repetidos++;
        }
        if(c == 2)c++;
        if(numeros[2] == numeros[c])
        {
            contador_repetidos++;
        }
        if(d == 3)d++;
        if(numeros[3] == numeros[d])
        {
            contador_repetidos++;
        }
        if(e == 4)e++;
        if(numeros[4] == numeros[e])
        {
            contador_repetidos++;
        }
        if(f == 5)f++;
        if(numeros[5] == numeros[f])
        {
            contador_repetidos++;
        }
        if(g == 6)g++;
        if(numeros[6] == numeros[g])
        {
            contador_repetidos++;
        }
        if(h == 7)h++;
        if(numeros[7] == numeros[h])
        {
            contador_repetidos++;
        }
        if(j == 8)j++;
        if(numeros[8] == numeros[j])
        {
            contador_repetidos++;
        }
        if(k == 9)k++;
        if(numeros[9] == numeros[k])
        {
            contador_repetidos++;
        }
        if(a == 10 || b == 10 || c == 10 || d == 10 || e == 10 || f == 10 || g == 10 || h == 10 || j == 10 || k == 10)
        {
            break;
        }
        a++;
        b++;
        c++;
        d++;
        e++;
        f++;
        g++;
        h++;
        j++;
        k++;
        printf("\n%d\n", numeros[i]);
    }
    return(0);
}