/* 1) Permita el ingreso de 5 números enteros y guárdelos en un vector. 
Muestre el vector en pantalla, al derecho y al revés. */
#include <stdio.h>
int main()
{
    int numeros, i = 0; 
    int datos [5];
    for(i = 0; i < 5; i++)
    {
        scanf("%d", &numeros);
        datos[i] = numeros;
        printf("\n\n%d\n\n", datos[i]);
    }
    for(i = 4;i >= 0;i--)
    {
        printf("\n\n%d\n\n", datos[i]);
    }
    return(0);
}