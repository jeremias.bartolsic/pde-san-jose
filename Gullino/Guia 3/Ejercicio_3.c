/* Permita el ingreso de 5 flotantes. Muestre el vector en pantalla. Luego multiplique todos los valores por 1.65 y vuelva a mostrarlos. */
#include <stdio.h>
int main()
{
    float numeros [5];
    int i;
    float multiplicar = 0;
    for(i = 0; i < 5; i++)
    {
        numeros [i] = 0;
        scanf("%f", &numeros[i]);
        printf("\n%f\n", numeros[i]);
    }
    for(i = 0; i < 5; i++)
    {
        multiplicar = numeros[i] * 1.65;
        printf("\n%f\n", multiplicar);
    }
    return(0);
}