/* El usuario ingresa 5 edades (enteras) y 5 sueldos (float), correspondientes a 5 personas.
Muestre en pantalla los vectores con los valores ingresados.
Muestre en qué posición se encuentra la persona de más edad y en qué posición se encuentra el mayor sueldo.

b) Agregue al punto anterior un mensaje que diga si la persona más grande es también la que más gana, o no. */

#include <stdio.h>
int main()
{
    int edades [5];
    float sueldos [5];
    int i = 0, mayor_edad = 0, mayor_sueldo = 0, orden_sueldos = 0, orden_edades = 0;
    for(i = 1; i <= 5; i++)
    {
        edades[i] = 0;
        sueldos[i] = 0;
        printf("\nIngrese edad y sueldo\n");
        scanf("%d", &edades[i]);
        scanf("%f", &sueldos[i]);
        printf("\n%d %f\n", edades[i], sueldos[i]);
        if(edades[i] > mayor_edad)
        {
            mayor_edad = edades[i];
            orden_edades = i;
        }
        if(sueldos[i] > mayor_sueldo)
        {
            mayor_sueldo = sueldos[i];
            orden_sueldos = i;
        }
    }
    if(orden_edades == orden_sueldos)
    {
        printf("\nEl usuario de mayor edad tambien tiene el mayor sueldo\n");
    }
    printf("\nEl usuario de mayor edad se encuentra en la posicion %d\n", orden_edades);
    printf("\nEl usuario de mayor sueldo se encuentra en la posicion %d\n", orden_sueldos);
    return(0);
}