/* Se cuenta con dos vectores de 8 enteros cada uno. 
Estos vectores tienen DNIs de jugadores de tenis. 
Se pide ingresar los datos y mostrar en pantalla las parejas, armadas así: 
el primero de un vector con el último del otro, el segundo con el anteúltimo, etc. */
#include <stdio.h>
int main()
{
    int DNI[8], parejas[8], i = 0, a = 1, b = 8;
    for(i = 1; i <= 8; i++)
    {
        DNI[i] = 0;
        parejas[i] = 0;
        printf("\nIngrese DNI del jugador\n");
        scanf("%d", &DNI[i]);
        printf("\nIngrese DNI de la pareja\n");
        scanf("%d", &parejas[i]);
    }
    while(1)
    {
        printf("\n%d es pareja de %d\n", DNI[a], parejas[b]);
        a++;
        b--;
        if(a == 9 && b == 0)
        {
            break;
        }
    }
    return(0);
}