/* El usuario ingresa datos de 5 microprocesadores.
Se cuenta con su precio (float) y su velocidad en gigahertz (float).
Mostrar en pantalla si el microprocesador más caro es también el más rápido, o no. */
#include <stdio.h>
int main()
{
    float precio [5], frecuencia [5], mayor_precio = 0, mayor_frecuencia = 0;
    int orden_precio = 0, orden_frecuencia = 0, i = 1;
    for(i = 1; i <= 5; i++)
    {
        precio [i] = 0;
        frecuencia [i] = 0;
        printf("\nIngrese el precio del microprocesador\n");
        scanf("%f", &precio[i]);
        printf("\nIngrese la frecuencia en gigahertz del mismo\n");
        scanf("%f", &frecuencia[i]);
        if(precio[i] > mayor_precio)
        {
            mayor_precio = precio[i];
            orden_precio = i;
        }
        if(frecuencia[i] > mayor_frecuencia)
        {
            mayor_frecuencia = frecuencia[i];
            orden_frecuencia = i;
        }
    }
    if(orden_precio == orden_frecuencia)
    {
        printf("\nEl microprocesador de mayor precio tambien es el de mayor frecuencia\n");
    }
    return(0);
}
