/* Dado un vector con 5 enteros, imprima en pantalla si es monótono o no,
es decir si todos los números son crecientes, decrecientes o ninguno de los anteriores. */
#include <stdio.h>
int main()
{
    int numeros[5], i = 0, verificacion_cre = 0, verificacion_dec = 0;
    for(i = 0; i < 5; i++)
        {
            printf("\nIngrese valor %d\n", i + 1);
            scanf("%d", &numeros[i]);
        }
    i = 1;
    while(i < 5)
    {
        if(numeros[i] > numeros[i - 1])
            {
                verificacion_cre++;
            }
        if(numeros[i] < numeros[i - 1])
            {
                verificacion_dec++;
            }
        i++;    
    }
        if(verificacion_cre == 4)
        {
            printf("\nEl vector es monotono creciente\n");
        }
        if(verificacion_dec == 4)
        {
            printf("\nEl vector es monotono decreciente\n");
        }
        if(verificacion_dec != 4 && verificacion_cre != 4)
        {
            printf("\nEl vector no es monotono decreciente ni drecreciente\n");
        }
    return 0;
}