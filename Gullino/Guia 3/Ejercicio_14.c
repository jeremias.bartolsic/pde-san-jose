/* Dado un vector de 8 floats, imprima en pantalla si todos sus elementos son idénticos, o no. */
#include <stdio.h>
int main()
{
    int i = 0, contador_identico = 0;
    float numeros [8];
    for(i = 0; i < 8; i++)
    {
        numeros[i] = 0;
        printf("\nIngrese valor %d\n", i);
        scanf("%f", &numeros[i]);
    }
    i = 0;
    for(i = 0; i < 8; i++)
    {
        if(numeros[i] == numeros[0])
        {
            contador_identico++;
        }
    }
    if(contador_identico == 8)
    {
        printf("\nTodos sus elementos son identicos\n");
    }
    return(0);
}