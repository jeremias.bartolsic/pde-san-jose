/* Dados los vectores del ejercicio anterior, imprima todas las combinaciones.
 Por ejemplo: 1ro de A con 1ro de B, 1ro de A con 2do de B, 1ro de A con 3ro de B.... 
 2do de A con 1ro de B, 2do de A con 2do de B... etc. */
 #include <stdio.h>
 int main()
 {
    int A [3], B [10], i = 1, a = 1;
    for(a = 0; a < 3; a++)
    {
        A [a] = 0;
        printf("\nIngrese valor para A\n");
        scanf("%d", &A [a]);
    }
    for(i = 0; i < 10; i++)
    {
        B [i] = 0;
        printf("\nIngrese valor para B\n");
        scanf("%d", &B [i]);
    }
    i = 0;
    while(i < 10)
    {
        printf("\nPara el primer valor de A: %d y %d", A[0], B[i]);
        printf("\nPara el segundo valor de A: %d y %d", A[1], B[i]);
        printf("\nPara el tercer valor de A: %d y %d", A[2], B[i]);
        i++;
    }
    a = 0;
    while(a < 3)
    {
        printf("\nPara el primer valor de B: %d y %d", B[0], A[a]);
        printf("\nPara el segundo valor de B: %d y %d", B[1], A[a]);
        printf("\nPara el tercer valor de B: %d y %d", B[2], A[a]);
        printf("\nPara el cuarto valor de B: %d y %d", B[3], A[a]);
        printf("\nPara el quinto valor de B: %d y %d", B[4], A[a]);
        printf("\nPara el sexto valor de B: %d y %d", B[5], A[a]);
        printf("\nPara el septimo valor de B: %d y %d", B[6], A[a]);
        printf("\nPara el octavo valor de B: %d y %d", B[7], A[a]);
        printf("\nPara el noveno valor de B: %d y %d", B[8], A[a]);
        printf("\nPara el decimo valor de B: %d y %d", B[9], A[a]);
        a++;
    }
    return(0);
 }