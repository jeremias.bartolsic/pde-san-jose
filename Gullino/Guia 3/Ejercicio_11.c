/* Dado un vector de enteros A con 3 posiciones y otro B de 10 posiciones,
indique si todos los elementos de A se encuentran al menos una vez en B, o no. */
#include <stdio.h>
int main()
{
    int A [3], B [10], i = 0, a = 0;
    for(a = 0; a < 3; a++)
    {
        A [a] = 0;
        printf("\nIngrese valor para A\n");
        scanf("%d", &A [a]);
    }
    for(i = 0; i < 10; i++)
    {
        B [i] = 0;
        printf("\nIngrese valor para B\n");
        scanf("%d", &B [i]);
    }
    a = 1;
    i = 1;
    while(a < 3)
    {
        a++;
        i++;
        if(A[a] == B[i])
        {
            printf("\nUn elemento de A se ve replicado en B\n");
        }
        if(a > 3)
        {
            break;
        }
    }
    return(0);
}