/* En dos vectores se tienen los goles de dos equipos en diferentes partidos que tuvieron entre si. 
En la posición 0 del vector equipo A están los goles de un partido por ese equipo y en la misma posición del vector equipoB están los goles del otro equipo en ese mismo partido. 
En la posición 1 de ambos vectores tendremos los goles para otro partido, etc. 
Se pide permitir que el usuario cargue los datos y mostrar cuántos empates han ocurrido.

b) Al ejercicio anterior agregue que se muestre cuál equipo es el ganador (A ó B). 
El ganador final es el que más partidos ha ganado. 
Considere que pueden tener igual cantidad de partidos ganados, en cuyo caso se dictamina un empate. */

#include <stdio.h>
int main ()
{
    int A [2], B [2], goles_A = 0, goles_B = 0, i = 1, contador_empates = 0, contador_victorias_A = 0, contador_victorias_B = 0;
    for(i = 0; i < 2; i++)
    {
        printf("\nIngrese los goles del equipo A\n");
        scanf("%d", &A[i]);
        printf("\nIngrese los goles del equipo B\n");
        scanf("%d", &B[i]);
        if(A[i] == B[i])
        {
            contador_empates++;
        }
        if(A[i] > B[i])
        {
            contador_victorias_A++;
        }
        if(A[i] < B[i])
        {
            contador_victorias_B++;
        }
    }
    
    
    if(contador_victorias_A > contador_victorias_B)
    {
        printf("\nEl equipo A es el ganador definitivo\n");
    }
    if(contador_victorias_A < contador_victorias_B)
    {
        printf("\nEl equipo B es el ganador definitivo\n");
    }
    if(contador_victorias_A == contador_victorias_B)
    {
        printf("\nEl resultado final es un empate entre ambos equipos\n");
    }
    if(contador_empates == 1) printf("\nSe produjo 1 empate\n");
    else
    {
        printf("\nSe produjeron %d empates\n", contador_empates);
    }
    return 0;
}