/* Permita el ingreso de 10 enteros positivos. Recorra el vector y todo número par que encuentre reemplaceló por cero.
Muestre en pantalla el resultado. */
#include <stdio.h>
int main()
{
    int numeros [10];
    int par = 0, i = 0;
    for(i = 0; i < 10; i++)
    {
        numeros[i] = 0;
        printf("\nIngrese numero/s\n");
        scanf("%d", &numeros[i]);
    }
    for(i = 0; i < 10; i++)
    {
        if(numeros[i] % 2 == 0)
        {
            numeros[i] = 0;
        }
        printf("\n%d\n", numeros[i]);
    }

return(0);
}