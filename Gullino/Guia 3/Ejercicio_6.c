/* El usuario ingresa el DNI (entero) de 10 estudiantes y su calificación (float).
Muestre en pantalla el DNI de los estudiantes que aprobaron. */
#include <stdio.h>
int main()
{
    int dni [10], i = 0;
    float nota [10];
    for(i = 1; i <= 10; i++)
    {
        dni[i] = 0;
        nota[i] = 0;
        printf("\nIngrese el DNI del alumno\n");
        scanf("%d", &dni[i]);
        printf("\nIngrese la nota correspondiente\n");
        scanf("%f", &nota[i]);
        if(nota[i] >= 6)
        {
            printf("\nEl alumno de DNI %d aprobo la materia\n", dni[i]);
        }
    }
    return(0);
}