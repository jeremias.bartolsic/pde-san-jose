/* El usuario ingresará nombres de personas hasta que ingrese la palabra "FIN". No sabemos cuántos nombres ingresará.
Luego de finalizar el ingreso, mostrar en pantalla cuál es el primer nombre en orden alfabético de todos los ingresados.*/
#include <stdio.h>
#include <string.h>
int main()
{
    char nombre [50], nombre_print[50];
    int i = 0;
    nombre_print[0] = 'z';
    while(1)
    {
        printf("\nIngrese nombre\n");
        scanf("%s", nombre);
        
        if(strcmp(nombre, nombre_print) < 0) strcpy(nombre_print, nombre);
            
        if(strcmp(nombre,"FIN") == 0) break;
    }
    printf("\nEl primer nombre en orden alfabetico es %s\n", nombre_print);
    return 0;
}