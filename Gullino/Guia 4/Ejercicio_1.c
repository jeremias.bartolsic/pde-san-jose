/* Permitir que el usuario ingrese una palabra de hasta 20 letras.
Mostrar en pantalla cuántas letras tiene la palabra ingresada.
Por ejemplo "CASA" debe indicar 4 letras. */
#include <stdio.h>
int main()
{
    char palabra [20];
    int i = 0;
    printf("\nIngrese una palabra\n");
    scanf("%s", palabra);
    for(i = 0; palabra[i] != '\0'; i++){}
    printf("\nLa palabra ingresada tiene %d letras\n", i);
    return 0;
}