/* El usuario ingresa una palabra. Mostrar en pantalla cuántas letras A minúsculas contiene. */
#include <stdio.h>
int main()
{
    char palabra[10];
    int letras = 0, minus = 0, i = 0;
    printf("\nIngrese una palabra\n");
    scanf("%s", palabra);
    for(letras = 0; palabra[letras] != '\0'; letras++){}
    for(i = 0; i < letras; i++){if(palabra[i] == 97) minus++;}
    if(minus == 1) printf("\nSe ingreso %d A minuscula en la palabra\n", minus);
    if(minus > 1) printf("\nSe ingresaron %d A minusculas en la palabra\n", minus);
    if(minus == 0) printf("\nNo se ingreso ninguna A minuscula en la palabra\n");
    return 0;
}