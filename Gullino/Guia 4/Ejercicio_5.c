/* El usuario ingresa una palabra. Mostrar en pantalla cuántas vocales minúsculas y mayúsculas contiene. */
#include <stdio.h>
int main()
{
    char palabra[10];
    int letras = 0, vocs = 0, i = 0;
    scanf("%s", palabra);
    for(letras = 0; palabra[letras] != '\0'; letras++){}
    for(i = 0; i < letras; i++){if(palabra[i] == 65/*A*/ || palabra[i] == 69/*E*/ || palabra[i] == 73/*I*/ || palabra[i] == 79/*O*/ || palabra[i] == 85/*U*/ || palabra[i] == 97/*a*/ || palabra[i] == 101/*e*/ || palabra[i] == 105/*i*/ || palabra[i] == 111/*o*/ || palabra[i] == 117/*u*/) vocs++;}
    printf("\nSe ingresaron %d vocales en total\n", vocs);
    return 0;
}