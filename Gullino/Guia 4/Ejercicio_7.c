/* El usuario ingresará 5 nombres de personas y sus edades (número entero). 
Luego de finalizar el ingreso, muestre en pantalla el nombre de la persona más joven.
El ingreso se realiza de este modo: nombre y edad de la primera persona, luego nombre y edad de la segunda, etc...
Nota: no hay que almacenar todos los nombres y todas las notas. */
#include <stdio.h>
#include <string.h>
int main()
{
    int edad [5], i = 0, longitud, mas_joven = 0;
    char nombre [50], nombre_joven[50];
    for(i = 0; i < 5; i++)
    {
        scanf("%s", nombre);
        longitud = strlen(nombre);
        scanf("%d", &edad[i]);
        if(mas_joven == 0)
        {
            mas_joven = edad[i];
            strcpy(nombre_joven, nombre);
        }
        if(edad[i] < mas_joven)
        {
            mas_joven = edad[i];
            strcpy(nombre_joven, nombre);
        }
    }
    printf("\nLa persona mas joven fue %s con %d anios\n", nombre_joven, mas_joven);
    return(0);
}