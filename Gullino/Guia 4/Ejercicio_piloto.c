/* Permitir que el usuario ingrese una palabra de maximo 10 letras
a) Mostrar en pantalla cuantas letras ingreso.
b) Mostrar en pantalla al reves. */
#include<stdio.h>
#include<string.h>  // Libreria para usar strings
int main()
{
    char palabras [11];
    int i = 0, contador_letras = 0, longitud = 0; //strlen calcula la longitud del string exceptuando los valores nulos
    printf("\nIngrese una palabra\n");
    scanf("%s", palabras);
    longitud = strlen(palabras);    // Variable con el valor obtenido de strlen
    for(i = 0; i < strlen(palabras); i++)   // Punto A
    {
        if(palabras[i] != ' ')
        {
            contador_letras++;
        }
    }
    printf("\nSe ingresaron %d letras\n", contador_letras);
    for(i = longitud - 1; i >= 0; i--)  // Punto B
    {
        printf("%c", palabras[i]); // Usando %s da error, supongo que tendra algo que ver con el orden en que se imprime
    }
    return 0;
}