/* El usuario ingresará una palabra de hasta 10 letras. Se desea mostrarla en pantalla pero encriptada según el "Código César".
Esto consiste en reemplazar cada letra con la tercera consecutiva en el abecedario. Por ejemplo, "CASA" se convierte en "FDVD".
Tener en cuenta que las últimas letras deben volver al inicio, por ejemplo la Y se convierte B.
Este mecanismo se utilizaba en el Imperio Romano. */
#include <stdio.h>
#include <string.h>
int main ()
{
    char palabra [10];
    int i = 0, longitud;
    while(1)
    {
        scanf("%s", palabra);
        longitud = strlen(palabra);
        for(i = 0; i < longitud; i++)
        {
            if(palabra[i] == 'X') palabra[i] = 'A';
            if(palabra[i] == 'Y') palabra[i] = 'B';
            if(palabra[i] == 'Z') palabra[i] = 'C';
            if(palabra[i] == 'x') palabra[i] = 'a';
            if(palabra[i] == 'y') palabra[i] = 'b';
            if(palabra[i] == 'z') palabra[i] = 'c';
            if(palabra[i] != 'X' && palabra[i] != 'Y' && palabra[i] != 'Z' && palabra[i] != 'x' && palabra[i] != 'y' && palabra[i] != 'z')
            {   
                palabra[i]++;
                palabra[i]++;
                palabra[i]++;
            }
        }
        printf("\n%s\n", palabra);
        if(strcmp(palabra, "stop") == 0) break;
    }
    return 0;
}