/*El usuario ingresa dos strings. 
Mostrar en pantalla si son iguales o no, es decir, si tienen las mismas letras en las mismas posiciones. */
#include <stdio.h>
int main()
{
    char palabra1 [10], palabra2[10];
    int i = 0, letras1 = 0, letras2 = 0, cont_letras = 0;
    printf("\nIngrese la primera palabra\n");
    scanf("%s", palabra1);
    printf("\nIngrese la segunda palabra\n");
    scanf("%s", palabra2);
    for(letras1 = 0; palabra1[letras1]!= '\0'; letras1++){}
    for(letras2 = 0; palabra2[letras2]!= '\0'; letras2++){}
    if(letras1 == letras2)
    {
        for(i = 0; i < letras1; i++)
        {
            if(palabra1[i] == palabra2[i]) cont_letras++;
        }
        if(cont_letras == letras1) printf("\nAmbas palabras son iguales\n");
    }  
    return 0;
}