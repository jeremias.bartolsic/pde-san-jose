/* Permitir el ingreso de una palabra y mostrarla en pantalla al revés.
Por ejemplo, para "CASA" se debe mostrar "ASAC". */
#include <stdio.h>
#define MAX_CARACT 10
int main()
{
    char palabra [MAX_CARACT];
    int i = 0, letras = 0;
    printf("\nIngrese una palabra\n");
    scanf("%s", palabra);
    for(letras = 0; palabra[letras] != '\0'; letras++){}
    for(i = letras; i >= 0; i--){printf("%c", palabra[i]);}
    return 0;
}