/* Permita que el usuario escriba dos palabras, haciendo dos scanf y obteniendo dos strings.
Muestre en pantalla si las dos palabras ingresadas son iguales, o no.
Iguales quiere decir idénticas: mismas letras en las mismas posiciones. */
#include <stdio.h>
#include <string.h>
int main()
{
    char palabra1 [10], palabra2[10];
    int i = 0, contador_letras = 0, longitud_string2 = 0, longitud_string1 = 0;
    printf("\nIngrese la primer palabra\n");
    scanf("%s", palabra1);
    printf("\nIngrese la segunda palabra\n");
    scanf("%s", palabra2);
    longitud_string1 = strlen(palabra1);
    longitud_string2 = strlen(palabra2);
    if(longitud_string1 == longitud_string2)
    {
        for(i = 0; i < longitud_string1; i++)
        {
            if(palabra1[i] == palabra2[i])
            {
                contador_letras++;
            }
        }
        if(contador_letras == longitud_string1)
        {
            printf("\nAmbas palabras son iguales\n");
        }
    }
    return 0;
}