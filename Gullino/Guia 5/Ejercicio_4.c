/* Una ferretería tiene un listado de facturas emitidas en cierto año, con estos datos: número de factura, fecha de emisión (día y mes), nombre del cliente y monto total. 
Dado un vector de 10 facturas, se necesita mostrar en pantalla cuál fue el mejor mes (o sea, el de mayor dinero facturado). 
Los datos se pueden ingresar por teclado o dejarlos fijos en el programa para no perder tiempo en tipear datos.*/
#include <stdio.h>
#include <string.h>
#define CANT_FACTURAS 2
struct fecha
{
    int dia;
    int mes;
    int anio;
};
struct factura
{
    char nombre[50];
    int numero;
    float monto;
    struct fecha facturacion;
};
int main()
{
    struct factura facturas[CANT_FACTURAS];
    int i = 0, mejor_mes = 0;
    for(i = 0; i < CANT_FACTURAS; i++)
    {
        printf("\nIngrese su nombre\n");
        scanf("%s", facturas[i].nombre);
        printf("\nIngrese numero de factura\n");
        scanf("%d", &facturas[i].numero);
        printf("\nIngrese la fecha de emision\n");
        scanf("%d", &facturas[i].facturacion.dia);
        scanf("%d", &facturas[i].facturacion.mes);
        facturas[i].facturacion.anio = 2007;
        printf("\nIngrese monto\n");
        scanf("%f", &facturas[i].monto);
        if(facturas[i].monto>facturas[i-1].monto)
        {
            mejor_mes = facturas[i].facturacion.mes;
        }
    }
    printf("\nEl mejor mes fue el %d\n", mejor_mes);
    return 0;
}