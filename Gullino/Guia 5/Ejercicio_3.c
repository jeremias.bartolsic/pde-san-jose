/*Utilice el ejercicio 1, modificándolo para que las notas del estudiante estén en un vector de notas dentro de la estructura. 
Este vector de notas puede almacenar hasta 10 notas del alumno. 
Los lugares no utilizados se escriben con un valor -1 para indicar que ese lugar está vacío.
De esta manera, un estudiante puede haber rendido 2 exámenes, otro 4 y otro 10, para citar algunos ejemplos. 
Se pide conservar la funcionalidad del programa teniendo en cuenta esta nueva organización de la información. 
Por lo tanto, para mostrar el promedio del estudiante, habrá que considerar las notas que tiene en este vector de notas, teniendo cuidado en utilizar solamente las notas que existan y omitiendo los -1 que pueda haber. */
#include <stdio.h>
#include <string.h>
#define ESTUDIANTES 5
struct estudiante
{
    int DNI;
    float notas[10];
};
int main()
{
    struct estudiante a[ESTUDIANTES];
    int i = 0, j = 0, cant_notas = 0;
    float promedio = 0;
    for(i = 0; i < ESTUDIANTES; i++)
    {
        printf("\nIngrese DNI\n");

        scanf("%d", &a[i].DNI);

        for(j = 0; j < 10; j++)
        {
            printf("\nIngrese notas\n");
            scanf("%f", &a[i].notas[j]);
            if(a[i].notas[j] != -1)
            {
                cant_notas++;
            }
            if(a[i].notas[j] == -1)
            {
                a[i].notas[j] = 0;
            }
        }
        promedio = (a[i].notas[i] + a[i].notas[i+1])/cant_notas;
        printf("\nDNI:\n%d\n \nPromedio: \n%f\n", a->DNI, promedio);
    }
    return 0;
}