/* Una aerolinea tiene vuelos, los cuales poseen un código alfanumérico (ejemplo: AR1500), una ciudad de origen y una ciudad de destino. 
Ingrese 4 vuelos en un vector. 
Luego de ingresados los datos permita que el usuario escriba el nombre de una ciudad y muestre los vuelos que parten y los que arriban a esa ciudad. 
Si no hubiera vuelos para la ciudad ingresada debe mostrarse un mensaje de error. */
#include <stdio.h>
#include <string.h>
#define MAX_VUELOS 4
struct vuelo
{
    char codigo [7];
    char c_origen [50];
    char c_destino [50];
};
int main()
{
    struct vuelo vuelos[MAX_VUELOS];
    char ciudad[50];
    int i = 0;
    for(i = 0; i < MAX_VUELOS; i++)
    {
        printf("\nCodigo: ");
        scanf("%s", &vuelos[i].codigo);
        printf("\nOrigen: ");
        scanf("%s", &vuelos[i].c_origen);
        printf("\nDestino: ");
        scanf("%s", &vuelos[i].c_destino);
    }
    while(1)
    {
        printf("\nIngrese una ciudad para buscar su vuelo\n");
        scanf("%s", ciudad);
        if(strcmp(ciudad, "fin") == 0) break;
        printf("\nLos resultados son:\n");
        if(strcmp(ciudad, vuelos[0].c_origen) == 0 || strcmp(ciudad, vuelos[0].c_destino) == 0)
        {
            printf("\n%s desde %s hacia %s\n", vuelos[0].codigo, vuelos[0].c_origen, vuelos[0].c_destino);
        }
        if(strcmp(ciudad, vuelos[1].c_origen) == 0 || strcmp(ciudad, vuelos[1].c_destino) == 0)
        {
            printf("\n%s desde %s hacia %s\n", vuelos[1].codigo, vuelos[1].c_origen, vuelos[1].c_destino);
        }
        if(strcmp(ciudad, vuelos[2].c_origen) == 0 || strcmp(ciudad, vuelos[2].c_destino) == 0)
        {
            printf("\n%s desde %s hacia %s\n", vuelos[2].codigo, vuelos[2].c_origen, vuelos[2].c_destino);
        }
        if(strcmp(ciudad, vuelos[3].c_origen) == 0 || strcmp(ciudad, vuelos[3].c_destino) == 0)
        {
            printf("\n%s desde %s hacia %s\n", vuelos[3].codigo, vuelos[3].c_origen, vuelos[3].c_destino);
        }
    }
    return 0;
}
