/* Declare una estructura para almacenar datos de estudiantes (DNI y dos notas correspondientes a los dos cuatrimestres del año).
Haga un programa que permita ingresar los datos de 5 estudiantes en un vector de estas estructuras.
Luego de ingresar los datos se deben mostrar los DNI de cada estudiante y el promedio que tiene en sus exámenes. */
#include <stdio.h>
#include <string.h>
#define ESTUDIANTES 5
struct estudiante
{
    int DNI;
    float notas[2];
};
int main()
{
    struct estudiante a[ESTUDIANTES];
    int i = 0, j = 0;
    float promedio = 0;
    for(i = 0; i < ESTUDIANTES; i++)
    {
        printf("\nIngrese DNI\n");

        scanf("%d", &a[i].DNI);

        for(j = 0; j < 2; j++)
        {
            printf("\nIngrese notas\n");
            scanf("%f", &a[i].notas[j]);
        }

        promedio = (a[i].notas[0] + a[i].notas[1])/2;
        printf("\nDNI:\n%d\n \nPromedio: \n%f\n", a->DNI, promedio);
    }
    return 0;
}