/* Utilizando la función anterior, arme un programa que muestre los números primos entre 2 y 100. */

#include <stdio.h>
int primo();

int main()
{
    int numero;
    int numero_primo = 0;
    
    for(numero = 2; numero <= 100; numero++)
    {
        numero_primo = primo (numero);
        if(numero_primo != 0)
        {
            printf("\n%d\n", numero_primo);
        }
    }

    return(0);
}

int primo(int num)
{
    int contador_divisor = 0;
    int divisor;
    for(divisor = 1 ; divisor <= num ; divisor++)
    {
        if(num % divisor == 0)
        {
            contador_divisor++;
        }
    }
        if(contador_divisor == 2)
        {
            return num;
        }
        else
        {
            return 0;
        }
}