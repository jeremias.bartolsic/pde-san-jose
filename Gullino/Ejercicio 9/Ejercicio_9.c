/* Los números "perfectos" son aquellos que la suma de los divisores (excluyendo el número mismo) es igual al número.
 Por ejemplo el 6 es un "número perfecto" porque 1 + 2 + 3 = 6.
  Encuentre y muestre en pantalla 2 números perfectos más. */

#include <stdio.h>
int main()
{
    int numero = 6;
    int divisor = 2;
    int suma_divisores = 0;
    while(numero > divisor)
    {
        numero++;
        suma_divisores = suma_divisores + divisor;
        divisor++;
        if(numero % divisor == 0)
        {
            return numero;
        }
    }
    if(suma_divisores == numero)
        {
            printf("\n%d\n", numero);
        }
    return(0);
}