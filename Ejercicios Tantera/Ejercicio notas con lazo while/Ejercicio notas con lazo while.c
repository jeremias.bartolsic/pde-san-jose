
    /*Realizar un programa que solicite notas por teclado hasta que se ingresa una nota menor que cero
    Cuando se ingreso dicha nota, el programa debera informar maximo, minimo, promedio y cantidad de notas mayores que 6.
*/

//Variables

#include <stdio.h>
#define CANT_NOTAS 5
#define MAXIMO 10
#define MINIMO 1

int nota;
int apr = 0;
float suma , promedio;
int notas = 1;
int max = 0 , min = 11;
int cantnotas;

//Codigo

int main (void)
{
    cantnotas = 0;
    while (cantnotas < CANT_NOTAS)
    {
        printf("Ingrese la nota %d\n" , notas);
        scanf("%d", &nota);
        
        cantnotas++;
        
        if(nota >= MINIMO && nota <= MAXIMO)
        {
          notas++;    
          suma = nota + suma;
        
          if(nota >= 6)
          apr++;
          
          if(nota > max)
          max = nota;
        
          if(nota < min)
          min = nota; 
        }
         else
          return(0);
    }
    promedio = suma / CANT_NOTAS;
    printf("El promedio es: %f\nLa nota maxima es: %d\nLa nota minima es: %d\nPruebas aprobadas: %d\n", promedio, max, min, apr);
    return(0);
}    
