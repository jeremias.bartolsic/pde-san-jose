#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#define MAX_TEMP 10
int CelTOFah();
void ordenar();
void impr();

int main()
{
    FILE*puntero;
    puntero = fopen("Caldera.txt","a+");
    if(puntero == NULL)
    {
        printf("\nError\n"); 
        exit(1);
    }    
    char datos[100];
    char registro[100] = "\nSe obtuvieron las siguientes temperaturas:";
    int temperatura[MAX_TEMP], i = 0, fahrenheit[MAX_TEMP], w = 1, aux = 0;
    for(i = 0; i < MAX_TEMP; i++)
    {
        printf("\nIngrese temperatura\n");
        scanf("%d", &temperatura[i]);
    }
    ordenar(temperatura);
    for(i = 0; i < MAX_TEMP; i++)
    {
        fahrenheit[i] = CelTOFah(temperatura[i]);
    }
    fputs(registro, puntero);
    fputc('\n', puntero);
    for(i = 0; i < MAX_TEMP && w!=NULL; i++)
    {
        w = fahrenheit[i];
        aux = fahrenheit[i];
        fprintf(puntero, "Temperatura %d: %dºC, que equivale a %dºF", i+1, temperatura[i], aux);
        fputc('\n', puntero);
    }
    fputc('\n', puntero);
    fclose(puntero);
    getch();
}

int CelTOFah(int celsius)
{
    int fah;
    fah = (celsius*9/5)+32;
    return fah;
}

void ordenar(int grados[])
{
    int i, j, aux;
    for(i = 0; i < MAX_TEMP-1; i++)
    {
        for(j = 0; j < MAX_TEMP-i-1; j++)
        {
            if(grados[j] > grados[j+1])
            {
                aux = grados[j+1];
                grados[j+1] = grados[j];
                grados[j] = aux;
            }
        }
    }
}